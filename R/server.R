library(tidyverse)
# source("R/stat_fun.R", encoding = "UTF-8")

server = function(input, output){

	output$graphe = renderPlot({
		if(!is.null(input$choix_x) & !is.null(input$choix_y)){

			if(input$choix_x!=input$choix_y){

				test_x = "Sepal.Length"
				test_y = "Sepal.Width"
				baseGraphe = iris %>%
					select(abs = input$choix_x, ord = input$choix_y, everything())
				graphe = baseGraphe %>%
					ggplot(aes(x=abs, y=ord, col=Species))+
					geom_point()+
					labs(x=input$choix_x, y=input$choix_y)+
					theme_bw()

				plot(graphe)
			}
		}
	})

	output$tableau_recap = renderDataTable({
		stat_fun(iris)
	})

}
